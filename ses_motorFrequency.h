/*INCLUDES *******************************************************************/
#include <avr/io.h>
#include "ses_common.h"

/* DEFINES & MACROS **********************************************************/
static bool motorIsOn = false; //flag indicates whether motor is running or not

/* FUNCTION PROTOTYPES *******************************************************/
typedef void (*pMotorSpikeCallback)();

pMotorSpikeCallback motorSpikeCallback;

void setMotorCallback(pMotorSpikeCallback callback);

void motorFrequency_init();

uint16_t motorFrequency_getRecent();

uint16_t motorFrequency_getMedian();

