/* INCLUDES ******************************************************************/

#include "ses_common.h"
#include "ses_adc.h"

/* DEFINES & MACROS **********************************************************/

// SENSOR wiring on SES board
#define TEMP_SENSOR_PORT       		PORTF
#define TEMP_SENSOR_PIN         	2

#define LIGHT_SENSOR_PORT 			PORTF
#define LIGHT_SENSOR_PIN      		4

#define JOYSTICK_PORT           	PORTF
#define JOYSTICK_PIN      			5

#define MICROPHONE_PORT           	PORTF
#define MICROPHONE_PIN       		0

#define ADC_VREF_SRC                1.6

/* FUNCTION DEFINITION *******************************************************/
void adc_init(void) {

	DDR_REGISTER(TEMP_SENSOR_PORT) &= ~(1 << TEMP_SENSOR_PIN);
	PIN_REGISTER(TEMP_SENSOR_PORT) &= ~(1 << TEMP_SENSOR_PIN);

	DDR_REGISTER(LIGHT_SENSOR_PORT) &= ~(1 << LIGHT_SENSOR_PIN);
	PIN_REGISTER(LIGHT_SENSOR_PORT) &= ~(1 << LIGHT_SENSOR_PIN);

	DDR_REGISTER(JOYSTICK_PORT) &= ~(1 << JOYSTICK_PIN);
	PIN_REGISTER(JOYSTICK_PORT) &= ~(1 << JOYSTICK_PIN);

	DDR_REGISTER(MICROPHONE_PORT) &= ~(1 << MICROPHONE_PIN);
	PIN_REGISTER(MICROPHONE_PORT) &= ~(1 << MICROPHONE_PIN);

	PRR0 &= ~(1 << 0);

	ADMUX = 0b11000000;

	ADCSRA |= ADC_PRESCALE;

	ADCSRA |= 1 << 7;

	while(((ADCSRB >> 7) & 1) == 1){ // wait till adc is powered on

		}

		//ADCSRA |= 1 << 5; // we need 5th bit set to allow for automatic adc triggering

		//ADCSRA |= 1 << 6; // 6th bit starts conversions
}

/**
 * Read the raw ADC value of the given channel
 * @adc_channel The channel as element of the ADCChannels enum
 * @return The raw ADC value
 */
uint16_t adc_read(uint8_t adc_channel) {
	switch (adc_channel) {
	case ADC_MIC_NEG_CH:
		ADMUX |= 0;
		break;
	case ADC_MIC_POS_CH:
		ADMUX |= 1;
		break;
	case ADC_TEMP_CH:
		ADMUX |= 10;
		break;
	case ADC_RESERVED1_CH:
		ADMUX |= 11;
		break;
	case ADC_LIGHT_CH:
		ADMUX |= 100;
		break;
	case ADC_JOYSTICK_CH:
		ADMUX |= 101;
		break;
	case ADC_RESERVED2_CH:
		ADMUX |= 110;
		break;
	case ADC_RESERVED3_CH:
		ADMUX |= 111;
		break;

	default:
		return ADC_INVALID_CHANNEL;
	}

	ADCSRA |= 1 << 6;

	while (1) {
		if (((ADCSRA >> 6) & 1) == 0) {
			return ADC;
		}
	}



}

/**
 * Read the current joystick direction
 * @return The direction as element of the JoystickDirections enum
 */
uint8_t adc_getJoystickDirection() {


	/*uint16_t myADCL = ADCL;
	uint16_t adcReading = ADCH;
	myADCL = myADCL >> 6;
	adcReading = adcReading << 2;
	adcReading |= myADCL;*/


	uint16_t adcREAD = adc_read(ADC_JOYSTICK_CH);

	if ( adcREAD >180 && adcREAD < 220)
	{
		return RIGHT;
	}
	else if(adcREAD >380 && adcREAD < 420){
		return UP;

	}
	else if(adcREAD >580 && adcREAD < 620){
		return LEFT;

	}
	else if(adcREAD >780 && adcREAD < 820){
		return DOWN;

	}
	else{
		return NO_DIRECTION;
	}

}


/**
 * Read the current temperature
 * @return Temperature in tenths of degree celsius
 */
//int16_t adc_getTemperature() {


//}
