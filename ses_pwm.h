/*INCLUDES *******************************************************************/
#include <avr/io.h>
#include "ses_common.h"

/*PROTOTYPES *****************************************************************/
void pwm_init(void);
void pwm_setDutyCycle(uint8_t dutyCycle);
