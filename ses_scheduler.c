/*INCLUDES ************************************************************/
#include "ses_timer.h"
#include "ses_scheduler.h"
#include "util/atomic.h"
#include "ses_led.h"

/* PRIVATE VARIABLES **************************************************/
/** list of scheduled tasks */
static taskDescriptor* taskList = NULL;
static systemTime_t milliseconds=0;// private variable to store millisecond value of the clock

/*FUNCTION DEFINITION *************************************************/
static void scheduler_update(void) {
	milliseconds++;
	taskDescriptor* current = taskList;
	while (current != NULL) {
		current->expire--;
		if (current->expire == 0) {
			current->execute = 1;
			if (current->period != 0) {
				current->expire = current->period;
			}
		}
		current = current->next;
	}
}

void scheduler_init() {
	timer2_setCallback(&scheduler_update);
	timer2_start();
}

void scheduler_run() {
	while (1) {
		/*ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		 {

		 while (current != NULL) {
		 if (current->expire <= 0) {
		 current->execute = 1;
		 }
		 current = current->next;
		 }
		 }*/
		taskDescriptor* current2 = taskList;
		while (current2 != NULL) {
			if (current2->execute == 1) {
				current2->task(current2->param);
				current2->execute = 0;
				if (current2->period == 0) {
					scheduler_remove(current2);
				}
			}
			current2 = current2->next;
		}
	}
}
bool scheduler_add(taskDescriptor * toAdd) {
	if (toAdd == NULL) {
		return false;
	}
	taskDescriptor* current = taskList;
	while (current != NULL) {
		if (current == toAdd) {
			return false;
		}
		current = current->next;
	}
	taskDescriptor * current2 = taskList;
	if (current2 == NULL) {
		taskList = toAdd;
		taskList->next = NULL;
		return true;
	}

	while (current2->next != NULL) {
		current2 = current2->next;
	}
	current2->next = toAdd;
	toAdd->next = NULL;

	return true;
}

void scheduler_remove(taskDescriptor * toRemove) {
	taskDescriptor* current = taskList;

	if (current == NULL) {
		return;
	}
	if (current->next == NULL && current == toRemove) {
		taskList = NULL;
	}
	if (current == toRemove) {
		taskList = current->next;
	}
	while (current->next != toRemove && current != NULL) {
		current = current->next;
	}
	if (current != NULL) {
		current->next = toRemove->next;
	}
}
