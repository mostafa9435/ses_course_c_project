/* INCLUDES ******************************************************************/
#include "ses_motorFrequency.h"
#include "ses_common.h"
#include "util/atomic.h"
#include "ses_timer.h"
#include "ses_led.h"
#include "math.h"
#include "ses_pwm.h"

/* DEFINES & MACROS **********************************************************/
#define SPIKE_PORT           	PORTD //set a name to the port D where a spike for the motor is read
#define SPIKE_PIN       	        0 //pin where the spike is read
float period; //variable to store time taken for a complete revolution
uint16_t current_frequency; //reciprocal of the period to get the frequency to return to the user
uint16_t drop_counter = 0; //to count the 6 current drops
#define  N                      	   10 //define an arbitrary size of array of frequency reads
float periods[N]; //array of previous frequency reads
uint16_t Index = 0; //array index for array 'periods'
#define KP             					2 //proportional gain
#define KI             					2 //integral gain
#define KD             					0 //derivative gain
#define AW           			  	65530 //saturation of errors
#define MAX_DUTY_CYCLE             	255 //maximum duty cycle to operate motor at maximum speed
//define the maximum amongst two variables 'x' & 'y'
#define MAX(x,y) ((x) >= (y)) ? (x) : (y)
#define MIN(x,y) ((x) <= (y)) ? (x) : (y)
uint16_t error = 0; //error(k)
uint16_t last_error = 0; //error(k-1)
uint16_t sum_of_errors = 0; //sigma errors
static uint16_t u = 0; //control input
static uint16_t max_u = 0; //maximum control input
uint16_t desiredMotorFrequency;

/* FUNCTION DEFINITION *******************************************************/
//callback function for when a spike from the motor is sensed
void setMotorCallback(pMotorSpikeCallback callback) {
	motorSpikeCallback = callback;
}

void motorFrequency_init() {
	//set PORTD0 as output pin
	DDR_REGISTER(SPIKE_PORT) &= ~(1 << SPIKE_PIN);
	//Setting Up interrupt for INT0
	EICRA |= (1 << ISC00);
	EICRA |= (1 << ISC01);
	//unmasking the interrupt bit for INT0 to allow for interrupts to generate respective ISR
	EIMSK |= (1 << INT0);
	//Initialize timer 5
	timer5_start();
}

//get frequency of motor
uint16_t motorFrequency_getRecent() {
	return current_frequency;
}

//get the median value of a bunch of motor frequencies
uint16_t motorFrequency_getMedian() {
	//atmoic block to disable interrupts to not allow changes in array elements during median calculation
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		//2 loop counters for nesting
		int i = 0, j = 0;
		float temp = 0;

		//sorting algorithm for an array
		for (i = 0; i < N; i++) {
			for (j = 0; j < N - 1; j++) {
				if (periods[j] > periods[j + 1]) {
					temp = periods[j];
					periods[j] = periods[j + 1];
					periods[j + 1] = temp;
				}
			}
		}
		if (periods[N / 2] != 0) {
			//if number of elements are even
			if (N % 2 == 0) {
				return (uint16_t) (1
						/ ((periods[(N - 1) / 2] + periods[N / 2]) / 2.0));
			}
			//if number of elements are odd
			else {
				return (uint16_t) (1 / (periods[N / 2]));
			}
		} else {
			return 0;
		}
	}
	return 0;
}

void setDesiredMotorFrequency(uint16_t motorFrequency) {
	desiredMotorFrequency = motorFrequency;
	//maximum input to motor
	max_u = KP * desiredMotorFrequency + KI * AW + KD * (last_error - error);
}

//automatic generation of required duty cycle
void aprropriateDutyCycle(void) {
	error = desiredMotorFrequency - motorFrequency_getRecent();
	sum_of_errors = MAX(MIN(sum_of_errors + error, AW), -AW);
	u = KP * error + KI * sum_of_errors + KD * (desiredMotorFrequency);
	last_error = error;
	if (motorIsOn == true) {
		//if motor is on
		//and input is negative
		if (u <= 0) {
			pwm_setDutyCycle(MAX_DUTY_CYCLE);
		} else {
			//and input is a non negative value
			pwm_setDutyCycle(((-OCR0A / max_u) * u) + MAX_DUTY_CYCLE);
		}
	}
}

//interrupt service routine for INT0
ISR(INT0_vect) {
	drop_counter++;
	EIFR |= (1 << INTF0);
	//call respective call back function for when a spike occurs
	motorSpikeCallback();
	led_greenOff();

	//wait for 6 consecutive drops in current
	if (drop_counter == (uint8_t) 6) {
		drop_counter = (uint8_t) 0;
		//16x10^6 / 256(prescaler) = 62500
		period = TCNT5 / 62500.0;
		periods[Index] = period;
		Index++;
		//if array is looped over, we reset the index back
		if (Index == N) {
			Index = 0;
		}
		current_frequency = (uint16_t) (roundf(1 / period));
		//Adjust PWM according to current frequency
		aprropriateDutyCycle();
		//reset timer 5 to restart the count
		TCNT5 = 0;
	}
}

//interrupt service routine for timer 5
ISR(TIMER5_COMPA_vect) {
	//clear compare match A flag
	TIFR5 |= 1 << OCF5A;
	current_frequency = 0;
	periods[Index] = 0;
	Index++;
	if (Index == N) {
		Index = 0;
	}
	//call respective call back function for when a compare match occurs
	timer5Callback();
	//reset timer 5 to restart the count
	TCNT5 = 0;
}
