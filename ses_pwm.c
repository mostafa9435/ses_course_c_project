/* INCLUDES ******************************************************************/
#include "ses_pwm.h"
#include "ses_timer.h"

/* DEFINES & MACROS **********************************************************/
#define TRANSISTOR_PORT           	PORTG
#define TRANSISTOR_PIN       	        5

/* FUNCTION DEFINITION *******************************************************/
void pwm_init(void) {
	//Data Direction Register (DDR) bit corresponding to the OC0B pin must be set
	DDR_REGISTER(TRANSISTOR_PORT) |= 1 << TRANSISTOR_PIN;
	//Initialize timer 0
	timer0_start();
	//debugging purposes, OCR0B works when OCR0A is set to that value
	OCR0A = 0xFF;
}

void pwm_setDutyCycle(uint8_t dutyCycle) {
	//timer 0 compare value for interrupt purposes
	OCR0B = dutyCycle;
}
