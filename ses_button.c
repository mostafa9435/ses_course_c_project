/* INCLUDES ******************************************************************/

#include "ses_common.h"
#include "ses_button.h"

/* DEFINES & MACROS **********************************************************/

// LED wiring on SES board
#define ROTARY_ENCODER_PORT           	PORTB
#define ROTARY_ENCODER_PIN       	        6

#define JOYSTICK_PORT       	        PORTB
#define JOYSTICK_PIN     	                7

#define BUTTON_NUM_DEBOUNCE_CHECKS          5

void button_checkState();

void button_init(bool debouncing) {

	DDR_REGISTER(ROTARY_ENCODER_PORT) &= 0 << ROTARY_ENCODER_PIN;
	DDR_REGISTER(JOYSTICK_PORT) &= 0 << JOYSTICK_PIN;

	PIN_REGISTER(ROTARY_ENCODER_PORT) |= 1 << ROTARY_ENCODER_PIN;
	PIN_REGISTER(JOYSTICK_PORT) |= 1 << JOYSTICK_PIN;

	if (debouncing == true) {
		// initialization for debouncing
		//timer1_setCallback(&button_checkState);
		//button_checkState();
	}

	else {
		//  initialization for direct interrupts (e.g. setting up the PCICR register)

		//PCIE1 & PCIE0 are set
		PCICR |= 11 << PCIE0;

		//set bits 7 (JOYSTICK_PIN ) and 6 (ROTARY_ENCODER_PIN) to enable triggering
		PCMSK0 |= 11 < PCINT6;
	}
}

bool button_isJoystickPressed(void) {
	if ((( PINB >> JOYSTICK_PIN) & 1) == 0) {
		return true;
	} else {
		return false;
	}
}

bool button_isRotaryPressed(void) {
	if (((PINB >> ROTARY_ENCODER_PIN) & 1) == 0) {
		return true;
	} else {
		return false;
	}
}

void button_setRotaryButtonCallback(pButtonCallback callback) {
	rotaryCallback = callback;


}

void button_setJoystickButtonCallback(pButtonCallback callback) {
	joystickCallback = callback;

}

void button_checkState() {

	static uint8_t state[BUTTON_NUM_DEBOUNCE_CHECKS] = { };
	static uint8_t index = 0;
	static uint8_t debouncedState = 0;
	uint8_t lastDebouncedState = debouncedState;
// each bit in every state byte represents one button
	state[index] = 0;

	if (button_isJoystickPressed()) {
		state[index] |= 1;
	}

	if (button_isRotaryPressed()) {
		state[index] |= 2;
	}

	index++;

	if (index == BUTTON_NUM_DEBOUNCE_CHECKS) {
		index = 0;
	}
// init compare value and compare with ALL reads, only if
// we read BUTTON_NUM_DEBOUNCE_CHECKS consistent "1" in the state
// array, the button at this position is considered pressed

	uint8_t j = 0xFF;

	for (uint8_t i = 0; i < BUTTON_NUM_DEBOUNCE_CHECKS; i++) {
		j = j & state[i];
	}

	debouncedState = j;

//  extend function

	if (((debouncedState & 1) == 1) && (lastDebouncedState != debouncedState)) {
		joystickCallback();
	}

	if ((((debouncedState >> 1) & 1) == 1)
			&& (lastDebouncedState != debouncedState)) {
		rotaryCallback();
	}

}

ISR(PCINT0_vect) {
	if (button_isJoystickPressed()) {
		joystickCallback();

	}

	if (button_isRotaryPressed()) {
		rotaryCallback();

	}

}
