/* INCLUDES ******************************************************************/
#include "ses_timer.h"

/* DEFINES & MACROS **********************************************************/
#define TIMER1_CYC_FOR_5MILLISEC   1245//TODO
#define TIMER2_CYC_FOR_1MILLISEC	249//TODO

/*FUNCTION DEFINITION ********************************************************/
void timer2_setCallback(pTimerCallback cb) {
	//assign respective callback function for timer 2 interrupt
	timer2Callback = cb;
}

void timer2_start() {
	//start timer 2
	PRR0 &= ~(1 << PRTIM2);
	//setting WGM 21 to 1  bit1 to 1
	TCCR2A |= 1 << WGM21;
	//setting WGM 20 to 0  bit0 to 0
	TCCR2A &= ~(1 << WGM20);
	//setting WGM 22 to 0 bit3 to 0 ----> CTC
	TCCR2B &= ~(1 << WGM22);
	//CS21:20 set to 0  bit0 and bit1 to 0
	TCCR2B &= ~(1 << CS21);
	TCCR2B &= ~(1 << CS20);
	//CS22 set to 0 bit2 to 1----> CS22:20 set to 0x04 64 prescale
	TCCR2B |= 1 << CS22;
	//set bit1 to 1 to allow for compare A
	TIMSK2 |= 1 << OCIE2A;
	TIFR2 |= (1 << OCF2A);
	//compare to 250 to reach 1 millisecond
	OCR2A = TIMER2_CYC_FOR_1MILLISEC;
}

void timer2_stop() {
	//stop compare to not go to interrupts,  clear bit1
	PRR0 |= 1 << PRTIM2;
}

void timer1_setCallback(pTimerCallback cb) {
	//assign respective callback function for timer 1 interrupt
	timer1Callback = cb;
}

void timer1_start() {
	//start timer 1
	PRR0 &= ~(1 << PRTIM1);
	//setting WGM 11 to 1  bit1 to 1 // nooo set it to 0
	TCCR1A &= ~(1 << WGM11);
	//setting WGM 10 to 0  bit0 to 0 // nooo set it to 0
	TCCR1A &= ~(1 << WGM10);
	//setting WGM 12 to 0 bit3 to 0 // nooo set it to 0
	TCCR1B |= 1 << WGM12;
	//nooo set it to 0 ----> CTC = 0x04
	TCCR1B &= ~(1 << WGM13);
	TCCR1B |= 1 << CS11;
	TCCR1B |= 1 << CS10;
	TCCR1B &= ~(1 << CS12);
	//unmasking interrupt for timer 1
	TIMSK1 |= 1 << OCIE1A;
	//clearing the interrupt flag
	TIFR1 |= (1 << OCF1A);
	//compare to 1250 to reach 5 millisecond
	OCR1A = TIMER1_CYC_FOR_5MILLISEC;
}

void timer1_stop() {
	//stop compare to not go to interrupts
	PRR0 |= 1 << PRTIM1;
}

void timer0_setCallback(pTimerCallback cb) {
	//assign respective callback function for timer 0 interrupt
	timer0Callback = cb;
}

void timer0_start() {
	//start timer 0
	PRR0 &= ~(1 << PRTIM0);
	//setting WGM02:00 to 7 for fast pwm mode with TOP=OCRA (for TOP=0xFF set WGM to 3)
	TCCR0A |= 1 << WGM01;
	TCCR0A |= 1 << WGM00;
	TCCR0B |= 1 << WGM02;
	//CS02:00 = 0x01 no prescale
	TCCR0B &= ~(1 << CS02);
	TCCR0B &= ~(1 << CS01);
	TCCR0B |= 1 << CS00;
	//COM0B1:0 = 0x03 to set Oc0B on compare
	TCCR0A |= 1 << COM0B1;
	TCCR0A |= 1 << COM0B0;
	TCCR0B &= ~(1 << FOC0B);
	TCCR0B &= ~(1 << FOC0A);
}

void timer0_stop() {
	//stop compare to not go to interrupts
	PRR0 |= 1 << PRTIM0;
}

void timer5_setCallback(pTimerCallback cb) {
	//assign respective callback function for timer 5 interrupt
	timer5Callback = cb;
}

void timer5_start() {
	//start timer 5
	PRR1 &= ~(1 << PRTIM5);
	TIMSK5 |= 1 << OCIE5A;
	//CTC Mode
	TCCR5A &= ~(1 << WGM51);
	TCCR5A &= ~(1 << WGM50);
	TCCR5B |= (1 << WGM52);
	TCCR5B &= ~(1 << WGM53);
	//256 prescaler
	TCCR5B |= (1 << CS52);
	//compare value after 1 second without 6 consecutive sparks
	OCR5A = 62500;
}

void timer5_stop() {
	//stop compare interrupt
	PRR1 |= (1 << PRTIM5);
}

ISR(TIMER1_COMPA_vect) {
	//resetting the flag just in case REDUNDANT
	TIFR1 |= 1 << OCF1A;
	timer1Callback();
}

ISR(TIMER2_COMPA_vect) {
	//resetting the flag just in case REDUNDANT
	TIFR2 |= 1 << OCF2A;
	timer2Callback();
}

ISR(TIMER0_COMPA_vect) {
	//resetting the flag just in case REDUNDANT
	TIFR0 |= 1 << OCF0B;
	timer0Callback();
}

/*
  Use timer2   {check}
  Complete and use the macros given in ses_timer.c   {}
  Use Clear Timer on Compare Match (CTC) mode operation (chapter 21.5: Modes of Operation)  {WGM22:20 set to 0x02 to set CTC mode}
  Select a prescaler of 64 {CS22:20 set to 0x04}
  Set interrupt mask register for Compare A {TIMSK2, bit OCIE2A set to 1 to allow compariosn}
  Clear the interrupt flag by setting an 1 in flag register for Compare A {TIFR2, bit OCF2A set to 1 to clear flag after interrupt}
  Set a value in register OCR2A in order to generate an interrupt every 1 ms {set using macros}
 */

/*
  Use timer1   {check}
  Complete and use the macros given in ses_timer.c   {}
  Use Clear Timer on Compare Match (CTC) mode operation (chapter 21.5: Modes of Operation)  {WGM13:10 set to 0x04 to set CTC mode}
  Select a prescaler of 64 {CS12:10 set to 0x03}
  Set interrupt mask register for Compare A {TIMSK1, bit OCIE2A set to 1 to allow compariosn}
  Clear the interrupt flag by setting an 1 in flag register for Compare A {TIFR2, bit OCF2A set to 1 to clear flag after interrupt}
  Set a value in register OCR2A in order to generate an interrupt every 5 ms {set using macros}
 */
